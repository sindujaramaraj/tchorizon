/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This is a demonstration of the <Text> and <Image> components.
 *
 * @version 1.0
 * @author TCSASSEMBLER
 */

'use strict';

var React = require('react');
var ReactWebGL = require('react-webgl');

var WebGL = ReactWebGL.WebGL;
var Quad = ReactWebGL.Quad;
var Image = ReactWebGL.Image;
var Text = ReactWebGL.Text;

// Create the App component
var App = React.createClass({
    /**
     * Render the component.
     * @returns the component
     */
    render: function() {
        return (
            <WebGL logEnabled={true}>
                <Image src="img/image-1.png" x={10} y={10} width={400} height={400} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1}>
                    <Text x={10} y={10} width={812} height={32} backgroundColor="rgba(255, 0, 0, 1)" zIndex={1} fontSize="30px" fontFamily="Times New Roman">
                        This is Times New Roman, 30px, read background font text
                    </Text>
                    <Text x={10} y={40} width={800} height={30} backgroundColor="rgba(0, 0, 0, 0)" zIndex={-1} fontSize="14px" fontColor="rgba(255,0,0,0.5)" fontFamily="Arial">
                        This is Arial, 14px, red color, half transparent font text
                    </Text>
                    <Text x={10} y={70} width={200} height={30} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1} >
                        This is default font text
                    </Text>
                    <Text x={10} y={80} width={800} height={30} backgroundColor="rgba(0, 0, 0, 0)" zIndex={2} fontSize="24px" fontColor="rgba(0,255,0,0.3)" fontFamily="Lucida Console">
                        This is Lucida Console, 24px, blue color, 70% transparent font text
                    </Text>
                    <Text x={10} y={100} width={800} height={60} backgroundColor="rgba(0, 0, 0, 0)" zIndex={3} fontSize="40px" fontColor="rgba(0,0,0,1)" fontStyle="italic" fontFamily="Times New Roman">
                        This is italic, Times New Roman, 40px font text
                    </Text>
                </Image>
                <Image src="img/image-0.png" x={10} y={310} width={480} height={480} backgroundColor="rgba(0, 0, 0, 0)" zIndex={2}>
                    
                    <Image src="img/image-2.png" x={10} y={200} width={285} height={311} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1}>
                        <Image src="img/image-3.png" x={10} y={100} width={512} height={200} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1}>
                        </Image>
                        <Image src="img/image-4.png" x={10} y={150} width={512} height={200} backgroundColor="rgba(0, 0, 0, 0)" zIndex={2}>
                        </Image>
                    </Image>
                </Image>
                <Image src="img/image-5.png" x={600} y={10} width={256} height={256} backgroundColor="rgba(0, 0, 0, 0)" zIndex={3}>
                </Image>
                <Image src="img/image-0.jpg" x={600} y={300} width={256} height={256} backgroundColor="rgba(0, 0, 0, 0)" zIndex={4}>
                </Image>
                <Image src="img/image-1.jpg" x={600} y={500} width={256} height={256} backgroundColor="rgba(0, 0, 0, 0)" zIndex={5}>
                </Image>
            </WebGL>
        )
    }
});

React.render(<App/>, document.body);