/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This is a simple demonstration of the <WebGL> and <Quad> components.
 *
 * @version 1.0
 * @author albertwang
 */

'use strict';

var React = require('react');
var ReactWebGL = require('react-webgl');

var WebGL = ReactWebGL.WebGL;
var Quad = ReactWebGL.Quad;

var App = React.createClass({
    render: function() {
        return (
            <WebGL logEnabled={true}>
                <Quad width={400} height={400} backgroundColor="rgba(255, 0, 0, 0.5)" zIndex={1}>
                    <Quad x={100} y={100} width={200} height={200} backgroundColor="rgba(0, 255, 0, 0.5)" zIndex={3}>
                    </Quad>
                    <Quad x={150} y={150} width={200} height={200} backgroundColor="rgba(0, 0, 255, 0.5)" zIndex={2}>
                    </Quad>
                </Quad>
                <Quad x={200} y={200} width={400} height={400} backgroundColor="rgba(0, 0, 0, 0.5)" zIndex={0}>
                    <Quad x={100} y={100} width={200} height={200} backgroundColor="rgba(76, 230, 10, 0.8)" zIndex={1}>
                    </Quad>
                </Quad>
            </WebGL>
        )
    }
});

React.render(<App/>, document.body);