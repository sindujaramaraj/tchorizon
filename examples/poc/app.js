/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This is a performance demonstration of the <WebGL>, <Quad>, <Text> and <Image> components.
 *
 * @version 1.0
 * @author TCSASSEMBLER
 */

'use strict';

var React = require('react');
var ReactUpdates = require('react/lib/ReactUpdates');

var ReactWebGL = require('react-webgl');
var Section = require('./Section');

var WebGL = ReactWebGL.WebGL;
var Quad = ReactWebGL.Quad;
var Image = ReactWebGL.Image;
var Text = ReactWebGL.Text;

var ROW_HEIGHT = 450;

var COLUMN_WIDTH = 205;

// Create the App component
var App = React.createClass({
    /**
     * Render the component.
     * @returns the component
     */
    render: function() {
        var current = new Date ();
        var days = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
        var months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
        var time = (current.getHours () < 10 ? "0" : "") + current.getHours () + ":";
        time += (current.getMinutes () < 10 ? "0" : "") + current.getMinutes ();
        var date = days[current.getDay ()] + " " + (current.getDate () < 10 ? "0" : "") + current.getDate () + " " 
            + months[current.getMonth ()];

        var json = this.props.data ? JSON.parse(this.props.data) : {"toppicks": []};
        this._section = new Section({x: -304, y: 220, rowHeight: ROW_HEIGHT, columnWidth: COLUMN_WIDTH, 
            toppicks: json.toppicks});

        var sectionElement = this._section.createElement();
        
        return (
            <WebGL ref="webGL" logEnabled={true} >
                <Image src="i/bg-mist.png" x={0} y={0} width={1280} height={720} backgroundColor="rgba(0, 0, 0, 0)" 
                    zIndex={0} />
                <Quad width={1280} height={160} x={0} y={0} backgroundColor="rgba(55, 55, 55, 1)" zIndex={2} >
                    <Text x={84} y={29} width={200} height={30} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1} 
                        fontSize="22px" fontWeight="300" fontColor={"rgba(255, 255, 255, 1)"}>
                        ON DEMAND
                    </Text>
                    <Text x={1135} y={26} width={200} height={30} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1} 
                        fontSize="14px" fontWeight="300" fontColor={"rgba(255, 255, 255, 0.699999988079071)"}>
                        {time}
                    </Text>
                    <Text x={1090} y={45} width={200} height={30} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1} 
                        fontSize="14px" fontWeight="300" fontColor={"rgba(255, 255, 255, 0.699999988079071)"}>
                        {date}
                    </Text>

                    <Quad width={1100} height={40} x={84} y={76} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1}>
                        <Text x={0} y={0} width={250} height={38} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1} 
                            fontSize="30px" fontWeight="bold" fontColor={"rgba(255, 255, 255, 1)"}>
                            TOP PICKS
                        </Text>
                        <Text x={210} y={0} width={250} height={38} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1} 
                            fontSize="30px" fontWeight="200" fontColor={"rgba(255, 255, 255, 1)"}>
                            REPLAY
                        </Text>
                        <Text x={380} y={0} width={250} height={38} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1} 
                            fontSize="30px" fontWeight="200" fontColor={"rgba(255, 255, 255, 1)"}>
                            MY PRIME
                        </Text>
                        <Text x={575} y={0} width={250} height={38} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1} 
                            fontSize="30px" fontWeight="200" fontColor={"rgba(255, 255, 255, 1)"}>
                            VIDEO STORE
                        </Text>
                        <Text x={820} y={0} width={250} height={38} backgroundColor="rgba(0, 0, 0, 0)" zIndex={1} 
                            fontSize="30px" fontWeight="200" fontColor={"rgba(255, 255, 255, 1)"}>
                            MY CONTENT
                        </Text>
                    </Quad>
                </Quad>
                
                {sectionElement}
            </WebGL>
        );
    },

    /**
     * Set up scheduled task to dynamically add <Quad>, <Text> or <Image> elements to the <WebGL> element.
     */
    componentDidMount: function() {
        var self = this;
        var request = new XMLHttpRequest();
        request.open( 'GET', 'data/toppicks.json', true);
        request.addEventListener('load', function (event) {
            self.setProps ({"data": this.response}, function () {
                var webGL = self.refs.webGL;
                var sectionNode = webGL.node.getChildById('section');
                self._section.attachRenderNode(sectionNode);
            });
        }, false);
        request.send(null);
        
        var self = this;
        document.onkeydown = function (e) {
            var keyCode = e.which;
            var webGL = self.refs.webGL;
            var section = self._section;
            if (self._animating || (keyCode == 38 && !section.couldMoveUp()) ||
                (keyCode == 40 && !section.couldMoveDown())) {
                return;
            }

            switch (keyCode) {
            case 38: // Move up.
            case 40: // Move down.
                {
                    var startOffset = section.getOffsetY();
                    self._animating = true;
                    webGL.animate({offset: 0}, {offset: (keyCode == 38 ? ROW_HEIGHT : -ROW_HEIGHT)}, 500, 
                    function () {
                        self._animating = false;
                    }, function () {
                        section.moveTo(0, startOffset + this.offset);
                    });
                }
                break;

            case 37: // Move left.
            case 39: // Move right.
                {
                    var activeRow = section.getActiveRow();
                    var startOffset = activeRow.getOffsetX();
                    self._animating = true;
                    webGL.animate({offset: 0}, {offset: (keyCode == 37 ? COLUMN_WIDTH : -COLUMN_WIDTH)}, 400, 
                    function () {
                        self._animating = false;
                    }, function () {
                        activeRow.moveTo(startOffset + this.offset, 0);
                    });
                }
                break;

            default:
                break;
            }
        };
    }

});

React.render(<App />, document.body);

