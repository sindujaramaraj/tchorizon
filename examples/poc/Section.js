/*
 * Copyright (C) 2015 TopCoder Inc., All Rights Reserved.
 */

/**
 * This class defines the section which composes with rows in the application.
 *
 * @providesModule Section
 * @version 1.0
 * @author TCSASSEMBLER
 */
'use strict';

var React = require('react');
var ReactWebGL = require('react-webgl');

var Row = require('./Row');

var Quad = ReactWebGL.Quad;

/**
 * Constructor for Section
 */
function Section (options) {
	this._rows = [];
	this._node = null;
	this._initX = options.x;
	this._initY = options.y;
	this._rowHeight = options.rowHeight;
	this._columnWidth = options.columnWidth;
	this._activeRowIndex = 0;
	this._offsetX = 0;
	this._offsetY = 0;

	var i, j, toppicks = options.toppicks, item, options;
	for (i = 0; i < toppicks.length; i += 1) {
		var row = new Row({x: 0, y: i * this._rowHeight, height: this._rowHeight, 
			cellWidth: this._columnWidth, items: toppicks[i].items});
		this._rows.push(row);
	}
}

/**
 * Creates the react element for this section
 * @return the reacte element
 */
Section.prototype.createElement = function () {
	var cellElements = [];
	this._rows.forEach(function (row) {
		row.getCells().forEach(function(cell) {
			cellElements.push(cell.createElement());
		});
	});

	return <Quad id="section" width={1280} height={720} x={this._initX} y={this._initY} 
    	backgroundColor="rgba(0, 0, 0, 0)" zIndex={1} >
            {cellElements}
        </Quad>;
}

/**
 * Move this section to the specified offset
 * @params offsetX the offset x
 * @params offsetY the offset y
 */
Section.prototype.moveTo = function (offsetX, offsetY) {
	var oldIndex = this._activeRowIndex;

	this._node.x = this._initX + offsetX;
	this._node.y = this._initY + offsetY;
	this._activeRowIndex = parseInt (-offsetY / this._rowHeight);
	this._offsetX = offsetX;
	this._offsetY = offsetY;

	if (oldIndex != this._activeRowIndex) {
		this._rows[oldIndex].setSelected(false);
		this._rows[this._activeRowIndex].setSelected(true);
	}
}

/**
 * Gets whether this section could move up
 * @return whether this section could move up
 */
Section.prototype.couldMoveUp = function () {
	return this._offsetY < 0;
}

/**
 * Gets whether this section could move down
 * @return whether this section could move down
 */
Section.prototype.couldMoveDown = function () {
	return this._offsetY > - this._rowHeight * 3;
}

/**
 * Namesake instance variable getter
 * @return the instance variable
 */
Section.prototype.getOffsetX = function () {
	return this._offsetX;
}

/**
 * Namesake instance variable getter
 * @return the instance variable
 */
Section.prototype.getOffsetY = function () {
	return this._offsetY;
}

/**
 * Gets the current active row
 * @return the active row
 */
Section.prototype.getActiveRow = function () {
	return this._rows[this._activeRowIndex];
}

/**
 * Attachs render node to this section
 * @params node the render node to attach
 */
Section.prototype.attachRenderNode = function (node) {
	var index = 0;

	this._node = node;
	this._rows.forEach(function (row) {
		row.getCells().forEach(function(cell) {
			cell.attachRenderNode(node.children[index ++]);
		});
	});
}

module.exports = Section;